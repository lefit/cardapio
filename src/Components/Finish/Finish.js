import React from 'react';
import { GlobalContext } from '../../GlobalContext';
import styles from './Finish.module.css';
import CartItem from '../Home/Cart/CartItem';
import Frete from '../Checkout/Form/Fragments/Frete';
import Countdown from "react-countdown";

const Completionist = () => <span>You are good to go!</span>;

// Renderer callback with condition
const renderer = ({ hours, minutes, seconds, completed }) => {

    return (
      <span>
        {seconds}
      </span>
    );
 
};

const Finish = () => {
  const { cart, total, typeBuy, sendMessage} = React.useContext(GlobalContext);

  return (
    <div className={styles.finishContainer}>
      <div className={styles.orderFinish}>
        <ion-icon name="checkmark-circle"></ion-icon>
        <h2>Pedido finalizado</h2><br/>
      </div>
      
      <div className={styles.orderContainer}>
        <div className={styles.orderItems}>
          {cart.map((item) => <CartItem key={item.id} product={item} isFinish={true} />)}
        </div>
        {typeBuy === 'delivery'? (
            <Frete></Frete>
          ) : (
            ''
          )}
        <div className={styles.total}>
          <h4>Total</h4>
          <h4 className={styles.price}>R$ {total},00</h4>
        </div>
      </div>
      <center><p className={styles.orderStore}>Redirecionando para o Whatsapp em <Countdown date={Date.now() + 4000} renderer={renderer}/> segundos, por favor, aguarde... <br/>
   
      {/* <span onClick={sendMessage()}> Ou clique para iniciar a conversa.</span> */}
      </p></center>
      {typeBuy === "delivery" ? (
        <p className={styles.orderStore}>Seu pedido será entregue em até 60 minutos.</p>
      ) : (
        <p className={styles.orderStore}>Em 25 minutos seu pedido estará pronto para ser retirado em nossa loja.</p>
      )}
    </div>
  )
}

export default Finish;
