import React from 'react';

export const GlobalContext = React.createContext();

export const GlobalStorage = ({ children }) => {
  const [data, setData] = React.useState([]);
  const [listProducts, setListProducts] = React.useState([]);
  const [openCart, setOpenCart] = React.useState(false);
  const [openObs, setOpenObs] = React.useState(false);
  const [obs, setObs] = React.useState(null);
  const [idObs, setIdObs] = React.useState(null);
  const [cart, setCart] = React.useState([]);
  const [total, setTotal] = React.useState(0);
  const [user, setUser] = React.useState(null);
  const [typeBuy, setTypeBuy] = React.useState('');
  const [cep, setCep] = React.useState('');
  const [number, setNumber] = React.useState('');
  const [complement, setComplement] = React.useState('');
  const [typePayment, setTypePayment] = React.useState('');
  const [address, setAddress] = React.useState(null);
  const [order, setOrder] = React.useState('');
  const [msg, setMsg] = React.useState('');
  const [troco, setTroco] = React.useState('');
  //se for delivery
  const taxaDelivery = 5;
  let valorFinal = 0.00; 


  function addCart(item) {
    item.quantity = 1;
    item.currentPrice = item.price;
    item.isSelected = true;
    setCart(oldArray => [...oldArray, item]);
    setTypeBuy("");
    console.log("seting type buy");
  }

  // add note to cart item
  function addObs(id) {
    let indexItem = cart.map((e) => e.id).indexOf(id);
    let updatedCart = [...cart];
    updatedCart[indexItem].obs = obs;
    setCart(updatedCart);
  }

  function sumFrete() {
    console.log("FRETE");
    
    setTotal(getSubTotal() + taxaDelivery)
    console.log(total);
  }

  function sumStore() {
    console.log("LOJA");
    setTotal(getSubTotal())
    console.log(total);
  }

  function incrementItem(item) {
    let indexItem = cart.map((e) => e.id).indexOf(item.id);
    let updatedCart = [...cart];
    updatedCart[indexItem].quantity = updatedCart[indexItem].quantity + 1;
    updatedCart[indexItem].currentPrice = updatedCart[indexItem].currentPrice + updatedCart[indexItem].price;
    if (typeBuy === "delivery"){
      sumFrete()
    }
    if (typeBuy === "store"){
      sumStore()
    }
    setCart(updatedCart);
  }

  function decrementItem(item) {
    let indexItem = cart.map((e) => e.id).indexOf(item.id);
    let updatedCart = [...cart];
    updatedCart[indexItem].quantity = updatedCart[indexItem].quantity - 1;
    updatedCart[indexItem].currentPrice = updatedCart[indexItem].currentPrice - updatedCart[indexItem].price;
    if (typeBuy === "delivery"){
      sumFrete()
    }
    if (typeBuy === "store"){
      sumStore()
    }
    if (updatedCart[indexItem].quantity === 0) {
      updatedCart[indexItem].isSelected = false;
      updatedCart.splice(indexItem, 1);
      setCart(updatedCart);
    }
    setCart(updatedCart);
  }

  

  function getCurrentDate(){


    let newDate = new Date()
    let date_raw = newDate.getDate();
    let month_raw = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    var date, month
      
    if (date_raw<10)  {  date ="0"+date_raw.toString()} else {  date =date_raw.toString()}
    if (month_raw<10)  {  month ="0"+month_raw.toString()} else {  month =month_raw.toString()}

    let time = newDate.getHours() + ':' + (newDate.getMinutes() < 10 ? "0"+newDate.getMinutes() : newDate.getMinutes()) + ':' + (newDate.getSeconds() < 10 ? "0"+newDate.getSeconds() : newDate.getSeconds());
    
    return year + "/"+ month + "/"+ date + "  " + time;
  }

  function formataProdutos(){
    const produtoInfo = cart.map((item) => item.quantity + " X "  + item.title + "\n" +  "Valor: R$" + item.price + ".00\n" + "Observações: " + (item.obs === undefined ? "N/A" : item.obs) + "\n\n");
    let newProdutoInfo = produtoInfo.toString().replaceAll(",", "");
    return newProdutoInfo;
  }

  function getSubTotal(){
    let _currentPrice = 0;
    cart.forEach(value =>{
      _currentPrice += value.currentPrice;
    })
    return _currentPrice;
  }

  function getLocationMaps(){
    let endereco = order.address.rua + "," + order.address.cidade + "," + order.address.number;
    const enderecoEncoded = encodeURIComponent(endereco);
    return enderecoEncoded;
  }

  function sendMessage(){
    
    const currentData = getCurrentDate();
    const subTotal = getSubTotal();
    let nome = order.name;
    let tel = order.phone;

    console.log(nome);
    console.log(tel);

    const produtos = formataProdutos();

    let text = "";
    
    if (typeBuy === "delivery"){
      valorFinal = subTotal + taxaDelivery;
      console.log(getLocationMaps());
      text = `#### NOVO PEDIDO - ENTREGA ####\n\nRealizado em ${currentData}\n\nCliente: ${nome} \nContato: ${tel} \n\nEndereço de entrega: \n${order.address.rua} - N: ${order.address.number} \n${order.address.cidade} - ${order.address.uf}, \nBairro: ${order.address.bairro} \nComplemento: ${order.address.complement} \n Localização: http://maps.google.com/?q=${getLocationMaps()} \n\n------- ITENS DO PEDIDO -------\n\n${produtos} \n-------------------------------\nSUBTOTAL: R$ ${subTotal},00\nENTREGA: R$ ${taxaDelivery},00\nVALOR FINAL: R$ ${valorFinal},00\n\nPAGAMENTO:\n${order.payment} \n ${typePayment === "Dinheiro" & troco != ''  ? "Troco para: R$" : ""} ${troco}`
    }else{
      valorFinal = subTotal;
      text = `#### NOVO PEDIDO - RETIRADA ####\n\nRealizado em ${currentData}\n\nCliente: ${nome} \n${tel} \n\n------- ITENS DO PEDIDO -------\n\n${produtos} \n-------------------------------\n\nVALOR FINAL: R$ ${valorFinal},00\n\n`
    }

    setTroco('')

    const textEncoded = encodeURIComponent(text);
    window.location.assign("https://wa.me/5586981117484?text="+textEncoded)

  }

  const getProducts = async (category) => {
    const response = await fetch(`https://my-json-server.typicode.com/FonteneleLucas/jsonServerTest/${category}`);
    const json = await response.json();
    setData(json);
  }

  async function getCep(cep) {
    const response = await fetch(`https://viacep.com.br/ws/${cep}/json/`);
    const json = await response.json();
    setAddress({
      rua: json.logradouro,
      cidade: json.localidade,
      bairro: json.bairro,
      uf: json.uf,
      cep: json.cep
    });
  }

  // wait for the CEP to be filled in with 8 digits to make the request
  React.useEffect(() => {
    if (cep.length >= 8) {
      getCep(cep);
    }
  }, [cep])

  // watch the cart changes to calculate the total price.
  React.useEffect(() => {

    if (cart.length > 0 & typeBuy != "delivery" & typeBuy != "store") {
      setTotal(getSubTotal());
      console.log("teste 1")
    }
  
    if (cart.length > 0) {
      // setTotal(getSubTotal());

      console.log("Type Buy")
      console.log(typeBuy)
    }
  }, [cart]);

  React.useEffect(() => { 
    setTimeout(() => {
      if(typeBuy === "delivery" || typeBuy === "store"){
        console.log(order)
        sendMessage();
      }
    }, 4000);
    

  }, [order])

  // save the address in localStorage after finalizing the order
  React.useEffect(() => {
    if (typeBuy === 'delivery') {
      window.localStorage.setItem('address', JSON.stringify(order.address));
    }
  }, [order]);

  React.useEffect(() => {
    setListProducts(data);
  }, [data]);

  // loads product list and default localStorage address
  React.useEffect(() => {
    async function loadData() {
      const response = await fetch('https://my-json-server.typicode.com/FonteneleLucas/jsonServerTest/saladas');
      const json = await response.json();
      setData(json);
    }
    loadData();
    const addressDefault = window.localStorage.getItem('address');
    try {
      if (addressDefault !== '' && addressDefault !== null && addressDefault !== undefined) {
        setUser(JSON.parse(addressDefault));
      } else {
        setUser(null);
      }
    } catch (error) {
      
    }
    
  }, []);

  return (
    <GlobalContext.Provider value={{ getProducts, listProducts, addCart, cart, total, incrementItem, decrementItem, sumFrete, sumStore, user, setUser, typeBuy, setTypeBuy, cep, setCep, number, setNumber, complement, setComplement, typePayment, setTypePayment, address, setAddress, order, setOrder, msg, setMsg, sendMessage, valorFinal,  openCart, setOpenCart, openObs, setOpenObs, idObs, setIdObs, obs, setObs, addObs, troco, setTroco }}>
      {children}
    </GlobalContext.Provider>
  );
};