import React, { useState } from 'react';
import { GlobalContext } from '../../../../GlobalContext';
import styles from './Address.module.css';

const Pix = () => {

  let copyText = "034.307.363-31";

    const [isCopied, setIsCopied] = useState(false)  

    // This is the function we wrote earlier
    async function copyTextToClipboard(text) {
      if ('clipboard' in navigator) {
        return await navigator.clipboard.writeText(text);
      } else {
        return document.execCommand('copy', true, text);
      }
    }
  
    // onClick handler function for the copy button
    const handleCopyClick = () => {
      // Asynchronously call copyTextToClipboard
      copyTextToClipboard(copyText)
        .then(() => {
          // If successful, update the isCopied state value
          setIsCopied(true);
          setTimeout(() => {
            setIsCopied(false);
          }, 2000);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  
    return (
      <div>
        <span onClick={handleCopyClick} className={styles.label}>Andrea Oliveira da Silva<br/>034.307.363-31  

        <svg xmlns="http://www.w3.org/2000/svg"  style={{marginLeft:6}} width="16" height="16" fill="currentColor" className="bi bi-clipboard" viewBox="0 0 16 16">
            <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
             <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
           </svg>
        </span>
        <span >
          <span>{isCopied ? 'Chave copiada!' : ''}</span>
        </span>
      </div>
    );
  }

  // function alert(){
  //   console.log("Copiado");
  // }

  // return (
  //     <div>
        
  //       <span className={styles.label} onClick={() => {navigator.clipboard.writeText("072.071.763-92")} }>Lucas do Nascimento Fontenele - 072.071.763-92  
  //       <span>
  //         <svg xmlns="http://www.w3.org/2000/svg"  style={{marginLeft:6}} width="16" height="16" fill="currentColor" className="bi bi-clipboard" viewBox="0 0 16 16">
  //           <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
  //           <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
  //         </svg>
  //       </span>
  //       </span>
        

  //     </div>
   
  // )


export default Pix;