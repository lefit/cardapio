import React from 'react';
import { GlobalContext } from '../../../../GlobalContext';
import styles from '../../../Home/Cart/Cart.module.css';

const Frete = () => {
  const { cep, setCep, number, setNumber, complement, setComplement, address } = React.useContext(GlobalContext);
  const [error, setError] = React.useState(false);

  return (
    <div className={styles.total}>
      <h4>Taxa de entrega</h4>
      <h4 className={styles.price}>R$ 5,00</h4>
    </div>
  )
}

export default Frete;