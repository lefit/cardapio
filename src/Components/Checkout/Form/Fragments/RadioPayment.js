import React from 'react';
import { GlobalContext } from '../../../../GlobalContext';
import ObsItem from '../../../Home/Cart/ObsItem';
import Pix from './Pix';
import styles from './RadioPayment.module.css';
import Troco from './Troco';

const RadioPayment = () => {
  const { typePayment, setTypePayment } = React.useContext(GlobalContext);

  return (
    <div className={styles.radioPayment}>
      <label className={styles.label}>
        <input name="card" type="radio" value="Cartão" className={styles.radio} checked={typePayment === "Cartão"} onChange={({ target }) => setTypePayment(target.value)} />
        Cartão
      </label>
      <label className={styles.label}>
        <input name="money" type="radio" value="Dinheiro" className={styles.radio} checked={typePayment === "Dinheiro"} onChange={({ target }) => setTypePayment(target.value)} />
        Dinheiro
      </label>
      <label className={styles.label}>
        <input name="money" type="radio" value="PIX" className={styles.radio} checked={typePayment === "PIX"} onChange={({ target }) => setTypePayment(target.value)} />
        PIX
      </label>
      <br/>
      <br/>
      <span className={styles.label}>
      {typePayment === "Dinheiro" ? <Troco></Troco>: ""}
      {typePayment === "PIX" ? <Pix></Pix>: ""}

    </span>
    </div>
   
    
  )
}

export default RadioPayment;