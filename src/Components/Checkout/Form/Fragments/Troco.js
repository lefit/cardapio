import React from 'react';
import { GlobalContext } from '../../../../GlobalContext';
import styles from './Address.module.css';

const Troco = () => {
  const { troco, setTroco } = React.useContext(GlobalContext);

  return (
      <div>
        <label className={styles.label}>Precisa de troco? se sim, para quanto?:
        <input id="troco" name="troco" className={styles.input} type="number" style={{width:'15vw', marginTop: 6}} onChange={({ target }) => setTroco(target.value)} />
        </label>
      </div>
   
  )
}

export default Troco;