import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './HomeNav.module.css';
import { ReactComponent as DrinksIcon } from '../../../Assets/drinks-icon.svg';

const HomeNav = () => {
  return (
    <nav className={styles.nav}>
      <NavLink to="/Saladas" activeClassName={styles.active}>
        <DrinksIcon />
        Saladas
      </NavLink>
    </nav>
  );
};

export default HomeNav;